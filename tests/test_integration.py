# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""
Integration tests.

Test connection to AWS bucket with and without Proxy.
Following environment variables needed:
* RUN_INTEGRATION_TESTS: Any value works.
* AWS_SECRET_ACCESS_KEY: Overwrites configuration key value pair to keep value secret.
* AWS_ACCESS_KEY_ID: Overwrites configuration key value pair to keep value secret.
* AWS_BUCKET: Overwrites configuration key value pair to keep value secret.
* PROXY_URL: Url to proxy server.
"""
import os
from datetime import datetime
from io import BytesIO
from pathlib import Path
from unittest.mock import MagicMock

import botocore
import pytest
import sqlalchemy
from sync_client.client_app import SyncClient
from sync_client.config import constants
from sync_client.data_models import DataLocation, DataPayload
from sync_client.io import io_s3
from sync_client.scheduler import RetryTaskScheduler

pytestmark = pytest.mark.skipif(
    condition=os.getenv("RUN_INTEGRATION_TESTS") is None,
    reason="integration test only work with dependent services.",
)

PATH_TEST_DATA = os.path.join(  # noqa: PTH118
    Path.cwd(), "tests", "integration_test_data"
)


@pytest.fixture()
def mock_db_engine():
    return MagicMock(spec=sqlalchemy.Engine)


@pytest.fixture()
def client(mock_db_engine, tmp_path):
    """Create a SyncClient instance."""
    s3_bucket = io_s3.S3Bucket(
        secret_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
        access_key=os.getenv("AWS_ACCESS_KEY_ID"),
        bucket_name=os.getenv("AWS_BUCKET"),
        aws_region_name=constants.aws_region,
    )
    locations_to_upload_to_s3: set[DataLocation] = {DataLocation.LocalFile}
    error_log_path = tmp_path / "error.log"
    error_log_path.write_text("ERROR LOG")
    return SyncClient(
        s3=s3_bucket,
        db_engine=mock_db_engine,
        sources=[],
        locations_to_upload_to_s3=locations_to_upload_to_s3,
        scheduler=RetryTaskScheduler(max_retries=1),
        error_log_path=error_log_path,
    )


@pytest.fixture()
def client_proxy(mock_db_engine, tmp_path):
    """
    Create a SyncClient instance with a Proxy configuration.

    Uses default certificates from certifi package (based on Mozilla CA bundle).
    """
    proxy_definitions = {
        "http": os.getenv("PROXY_URL"),
        "https": os.getenv("PROXY_URL"),
    }
    s3_bucket = io_s3.S3Bucket(
        secret_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
        access_key=os.getenv("AWS_ACCESS_KEY_ID"),
        bucket_name=os.getenv("AWS_BUCKET"),
        aws_region_name=constants.aws_region,
        proxy_definitions=proxy_definitions,
        proxies_config=None,
    )
    locations_to_upload_to_s3: set[DataLocation] = {DataLocation.LocalFile}
    error_log_path = tmp_path / "error.log"
    error_log_path.write_text("ERROR LOG")
    return SyncClient(
        s3=s3_bucket,
        db_engine=mock_db_engine,
        sources=[],
        locations_to_upload_to_s3=locations_to_upload_to_s3,
        scheduler=RetryTaskScheduler(max_retries=1),
        error_log_path=error_log_path,
    )


@pytest.fixture()
def client_proxy_ca_1(mock_db_engine, tmp_path):
    """
    Create a SyncClient instance with a Proxy configuration.

    Uses a custom certificate bundle based on Mozilla CA.
    """
    """"""
    proxy_definitions = {
        "http": os.getenv("PROXY_URL"),
        "https": os.getenv("PROXY_URL"),
    }
    bundle_path = os.path.join(PATH_TEST_DATA, "mozilla_bundle.crt")  # noqa: PTH118
    proxy_config = {
        "proxy_ca_bundle": bundle_path,
    }
    s3_bucket = io_s3.S3Bucket(
        secret_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
        access_key=os.getenv("AWS_ACCESS_KEY_ID"),
        bucket_name=os.getenv("AWS_BUCKET"),
        aws_region_name=constants.aws_region,
        proxy_definitions=proxy_definitions,
        proxies_config=proxy_config,
    )
    locations_to_upload_to_s3: set[DataLocation] = {DataLocation.LocalFile}
    error_log_path = tmp_path / "error.log"
    error_log_path.write_text("ERROR LOG")
    return SyncClient(
        s3=s3_bucket,
        db_engine=mock_db_engine,
        sources=[],
        locations_to_upload_to_s3=locations_to_upload_to_s3,
        scheduler=RetryTaskScheduler(max_retries=1),
        error_log_path=error_log_path,
    )


@pytest.fixture()
def client_proxy_ca_2(mock_db_engine, tmp_path):
    """
    Create a SyncClient instance with a Proxy configuration.

    Uses a minimum custom certificate bundle based S3 certification chain.
    """
    proxy_definitions = {
        "http": os.getenv("PROXY_URL"),
        "https": os.getenv("PROXY_URL"),
    }
    bundle_path = os.path.join(  # noqa: PTH118
        PATH_TEST_DATA, "aws-s3-eu-central-1.pem"
    )
    proxy_config = {
        "proxy_ca_bundle": bundle_path,
    }
    s3_bucket = io_s3.S3Bucket(
        secret_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
        access_key=os.getenv("AWS_ACCESS_KEY_ID"),
        bucket_name=os.getenv("AWS_BUCKET"),
        aws_region_name=constants.aws_region,
        proxy_definitions=proxy_definitions,
        proxies_config=proxy_config,
    )
    locations_to_upload_to_s3: set[DataLocation] = {DataLocation.LocalFile}
    error_log_path = tmp_path / "error.log"
    error_log_path.write_text("ERROR LOG")
    return SyncClient(
        s3=s3_bucket,
        db_engine=mock_db_engine,
        sources=[],
        locations_to_upload_to_s3=locations_to_upload_to_s3,
        scheduler=RetryTaskScheduler(max_retries=1),
        error_log_path=error_log_path,
    )


@pytest.fixture()
def client_proxy_bad_ca(mock_db_engine, tmp_path):
    """
    Create a SyncClient instance with a Proxy configuration.

    Uses a incomplete certificate bundle, which should raise SSLError.
    """
    proxy_definitions = {
        "http": os.getenv("PROXY_URL"),
        "https": os.getenv("PROXY_URL"),
    }
    bundle_path = os.path.join(PATH_TEST_DATA, "bad_bundle.crt")  # noqa: PTH118
    proxy_config = {
        "proxy_ca_bundle": bundle_path,
    }
    s3_bucket = io_s3.S3Bucket(
        secret_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
        access_key=os.getenv("AWS_ACCESS_KEY_ID"),
        bucket_name=os.getenv("AWS_BUCKET"),
        aws_region_name=constants.aws_region,
        proxy_definitions=proxy_definitions,
        proxies_config=proxy_config,
    )
    locations_to_upload_to_s3: set[DataLocation] = {DataLocation.LocalFile}
    error_log_path = tmp_path / "error.log"
    error_log_path.write_text("ERROR LOG")
    return SyncClient(
        s3=s3_bucket,
        db_engine=mock_db_engine,
        sources=[],
        locations_to_upload_to_s3=locations_to_upload_to_s3,
        scheduler=RetryTaskScheduler(max_retries=1),
        error_log_path=error_log_path,
    )


def test_connect_to_aws(client):
    """
    Test a live connection to a real AWS Bucket.

    See needed environment variables in module description.
    """
    local_data = BytesIO(
        f"Hello, World - {datetime.now()}".encode("utf-8")  # noqa: UP012
    )
    file_name = "test_upload.txt"
    payload = DataPayload(data=local_data, name=file_name, source=DataLocation.S3)

    client.upload_to_s3(payload, prefix=constants.ingest_folder_prefix)
    # get data from bucket
    contents = client.s3.s3.list_objects(Bucket=os.getenv("AWS_BUCKET"))["Contents"]
    remote_data = None
    for content in contents:
        if file_name in content.get("Key"):
            remote_data = content
            break

    remote_file_name = constants.ingest_folder_prefix + file_name
    assert remote_data, "no file with matching 'file_name'"
    assert client.s3._are_equal_local_and_remote(
        remote_file_name, local_data
    ), "file was not uploaded"


def test_connect_to_aws_with_proxy(client_proxy):
    """
    Test a live connection to a real AWS Bucket with a Proxy inbetween.

    See needed environment variables in module description.
    """
    local_data = BytesIO(
        f"Hello, World - {datetime.now()}".encode("utf-8")  # noqa: UP012
    )
    file_name = "test_upload.txt"
    payload = DataPayload(data=local_data, name=file_name, source=DataLocation.S3)

    client_proxy.upload_to_s3(payload, prefix=constants.ingest_folder_prefix)
    # get data from bucket
    contents = client_proxy.s3.s3.list_objects(Bucket=os.getenv("AWS_BUCKET"))[
        "Contents"
    ]
    remote_data = None
    for content in contents:
        if file_name in content.get("Key"):
            remote_data = content
            break

    remote_file_name = constants.ingest_folder_prefix + file_name
    assert remote_data, "no file with matching 'file_name'"
    assert client_proxy.s3._are_equal_local_and_remote(
        remote_file_name, local_data
    ), "file was not uploaded"


def test_connect_to_aws_with_proxy_and_ca_bundle_1(client_proxy_ca_1):
    """
    Test a live connection to a real AWS Bucket with a Proxy inbetween.

    HTTPS connection uses a custom certificate bundle based on Mozilla CA.
    See needed environment variables in module description.
    """
    local_data = BytesIO(
        f"Hello, World - {datetime.now()}".encode("utf-8")  # noqa: UP012
    )
    file_name = "test_upload.txt"
    payload = DataPayload(data=local_data, name=file_name, source=DataLocation.S3)

    client_proxy_ca_1.upload_to_s3(payload, prefix=constants.ingest_folder_prefix)
    # get data from bucket
    contents = client_proxy_ca_1.s3.s3.list_objects(Bucket=os.getenv("AWS_BUCKET"))[
        "Contents"
    ]
    remote_data = None
    for content in contents:
        if file_name in content.get("Key"):
            remote_data = content
            break

    remote_file_name = constants.ingest_folder_prefix + file_name
    assert remote_data, "no file with matching 'file_name'"
    assert client_proxy_ca_1.s3._are_equal_local_and_remote(
        remote_file_name, local_data
    ), "file was not uploaded"


def test_connect_to_aws_with_proxy_and_ca_bundle_2(client_proxy_ca_2):
    """
    Test a live connection to a real AWS Bucket with a Proxy inbetween.

    HTTPS connection uses a minimum custom certificate bundle
    based S3 certification chain.
    See needed environment variables in module description.
    """
    local_data = BytesIO(
        f"Hello, World - {datetime.now()}".encode("utf-8")  # noqa: UP012
    )
    file_name = "test_upload.txt"
    payload = DataPayload(data=local_data, name=file_name, source=DataLocation.S3)

    client_proxy_ca_2.upload_to_s3(payload, prefix=constants.ingest_folder_prefix)
    # get data from bucket
    contents = client_proxy_ca_2.s3.s3.list_objects(Bucket=os.getenv("AWS_BUCKET"))[
        "Contents"
    ]
    remote_data = None
    for content in contents:
        if file_name in content.get("Key"):
            remote_data = content
            break

    remote_file_name = constants.ingest_folder_prefix + file_name
    assert remote_data, "no file with matching 'file_name'"
    assert client_proxy_ca_2.s3._are_equal_local_and_remote(
        remote_file_name, local_data
    ), "file was not uploaded"


def test_connect_to_aws_with_proxy_and_bad_certificates(client_proxy_bad_ca):
    """
    Test a live connection to a real AWS Bucket with a Proxy inbetween.

    Incomplete certificate bundle should raise SSLError.
    See needed environment variables in module description.
    """
    """"""
    local_data = BytesIO(
        f"Hello, World - {datetime.now()}".encode("utf-8")  # noqa: UP012
    )
    file_name = "test_upload.txt"
    payload = DataPayload(data=local_data, name=file_name, source=DataLocation.S3)
    # botocore.exceptions.SSLError
    with pytest.raises(
        botocore.exceptions.SSLError,
        match="SSL validation failed*",
    ):
        client_proxy_bad_ca.upload_to_s3(payload, prefix=constants.ingest_folder_prefix)
