# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from sync_client.config import constants
from sync_client.data_models import DataLocation, DataPayload, S3File


def test_data_payload_from_file(tmp_path):
    content = b"test file content"
    tmp_file = tmp_path / "test_file.txt"
    tmp_file.write_bytes(content)
    name = "my_name"

    payload = DataPayload.from_file(tmp_file, name)
    assert payload.data.read() == content
    assert payload.name == name
    assert payload.source == DataLocation.LocalFile


@pytest.mark.parametrize(
    (
        "config_table_name",
        "expected_table_name",
        "expected_planning_unit",
        "expected_is_directory",
    ),
    [
        ("test", "test", None, False),
        ("testTable", "testTable", None, False),
        ("test_table", "test_table", None, False),
        ("schedule/unit_a", "schedule", "unit_a", True),
        ("ScheduleTasks/unit_b", "ScheduleTasks", "unit_b", True),
        ("Schedule_Tasks/unit-c", "Schedule_Tasks", "unit-c", True),
    ],
)
def test_s3file_init(
    config_table_name,
    expected_table_name,
    expected_planning_unit,
    expected_is_directory,
):
    """Check parsing of various configured table and file names."""
    expected_file_name = f"{constants.release_folder_prefix}{config_table_name}.parquet"
    file = S3File(table_name=config_table_name)

    assert file.file_name == expected_file_name
    assert file.table_name == expected_table_name
    assert file.planning_unit == expected_planning_unit
    assert file.is_directory == expected_is_directory
