# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
from unittest.mock import MagicMock

from sync_client.sources.event_handler import ModifiedFileHandler
from sync_client.sources.file_watcher import BruteForceObserver


def test_brute_force(tmp_path):
    """Check if brute force observer checks periodically."""
    event_handler = ModifiedFileHandler(
        directory_to_handle=tmp_path,
        included_filenames={"file1.txt", "file2.txt"},
        listener=None,
    )

    event_handler.notify = MagicMock()

    watch_interval_sec = 2  # Set your expected interval
    expected_checks = 2

    observer = BruteForceObserver(timeout=watch_interval_sec)
    observer.schedule(event_handler, tmp_path, recursive=False)
    observer.start()
    time.sleep(0.3)  # Give the observer time to start
    test_file = tmp_path / "file1.txt"
    test_file.write_text("Hello observer")
    time.sleep(expected_checks * watch_interval_sec)  # time to check

    observer.stop()

    # Check if the notify method is called within the expected time window
    calls = event_handler.notify.call_args_list
    assert len(calls) == expected_checks, "notify method not called"
