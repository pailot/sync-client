# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from io import BytesIO

import pyarrow.parquet as pq
import pytest
import sqlalchemy as sa
from dateutil import parser as date_parser
from sync_client.data_models import DataLocation, DataPayload
from sync_client.io import io_db
from sync_client.sources.db_trigger import DBTriggerSource, create_trigger_time_payload


@pytest.fixture()
def exclude_tables() -> list[str]:
    return ["TableX", "TableY"]


@pytest.fixture()
def exclude_partial_tables() -> list[str]:
    return ["Schedule/unit_a", "Schedule/unit_a", "TableA"]


@pytest.fixture()
def db_trigger(conn_str_to_tmp_db, trigger_table_name):
    return DBTriggerSource(
        engine=io_db.get_db_engine(conn_str_to_tmp_db),
        trigger_table_name=trigger_table_name,
        excluded_tables=None,
    )


def test_excluded_tables(conn_str_to_tmp_db, exclude_tables, trigger_table_name):
    """Check for exclusion of tables, when fetching."""
    source = DBTriggerSource(
        engine=io_db.get_db_engine(conn_str_to_tmp_db),
        trigger_table_name=trigger_table_name,
        excluded_tables=exclude_tables,
    )
    table_names = set(source.excluded_tables)
    expected_table_names = {*exclude_tables, trigger_table_name}

    difference = table_names.symmetric_difference(expected_table_names)
    assert not difference, "missing tables"


def test_excluded_partial_tables(
    conn_str_to_tmp_db, exclude_partial_tables, trigger_table_name
):
    """Check for exclusion of partial tables, when fetching."""
    source = DBTriggerSource(
        engine=io_db.get_db_engine(conn_str_to_tmp_db),
        trigger_table_name=trigger_table_name,
        excluded_tables=exclude_partial_tables,
    )
    table_names = set(source.excluded_tables)
    expected_table_names = {"Schedule", "TableA", trigger_table_name}

    difference = table_names.symmetric_difference(expected_table_names)
    assert not difference, "missing tables"


def test_check_trigger(db_trigger):
    # Add test data to the trigger table
    with db_trigger.db_engine.begin() as connection:
        insert_stmt = db_trigger.trigger_table.insert().values(
            **{db_trigger.trigger_column_name: True},
        )
        connection.execute(insert_stmt)

    is_triggered = db_trigger._is_trigger_active()
    assert is_triggered


def test_table_not_found_check_trigger(db_trigger, trigger_table_name):
    # Add test data to the trigger table
    with db_trigger.db_engine.begin() as connection:
        db_trigger.trigger_table.drop(connection)
    with pytest.raises(
        sa.exc.OperationalError,
        match=f"no such table: {trigger_table_name}",
    ):
        db_trigger._is_trigger_active()


def test_poll(db_trigger):
    """Check if polling resets the trigger and returns the data."""
    with db_trigger.db_engine.begin() as connection:
        insert_stmt = db_trigger.trigger_table.insert().values(
            **{db_trigger.trigger_column_name: True},
        )
        connection.execute(insert_stmt)

    result = db_trigger.poll()

    assert len(result) == 2
    assert result[0].name == "data_table.parquet"

    is_triggered = db_trigger._is_trigger_active()
    assert not is_triggered
    table_arrow = pq.read_table(result[0].data)
    assert table_arrow.column_names == ["id", "name"]


def test_create_trigger_time_log():
    # Test creating 'trigger_time_log' in-memory
    payload = create_trigger_time_payload()

    content = payload.data.read().decode("utf-8")
    timestamp = date_parser.parse(content)

    assert isinstance(payload, DataPayload)
    assert payload.name == "trigger_time.log"
    assert payload.source is DataLocation.Database
    assert isinstance(payload.data, BytesIO)
    assert isinstance(timestamp, datetime), "Content is no valid datetime"
    assert timestamp.tzinfo is not None, "No timezone found"
