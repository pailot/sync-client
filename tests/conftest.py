# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import contextlib

import pytest
import sqlalchemy as sa
from sync_client.config import constants


@pytest.fixture()
def trigger_table_name():
    return "trigger_table_name"


@pytest.fixture()
def conn_str_to_tmp_db(tmp_path, trigger_table_name):
    """Return the connection string to a tmp DB with data."""
    temp_db_file = tmp_path / "tmp.db"
    connection_string = f"sqlite:///{temp_db_file}"
    trigger_column_name = constants.trigger_column

    engine = sa.create_engine(connection_string, echo=True)
    metadata = sa.MetaData()

    trigger_table = sa.Table(
        trigger_table_name,
        metadata,
        sa.Column(trigger_column_name, sa.Boolean),
    )

    # a sample data table for testing read_all_tables_as_parquet()
    data_table_name = "data_table"
    data_table = sa.Table(
        data_table_name,
        metadata,
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(255)),
    )

    metadata.create_all(bind=engine)
    with engine.begin() as conn:
        conn.execute(data_table.insert(), [{"name": "Alice"}, {"name": "Bob"}])
        conn.execute(trigger_table.insert(), [{trigger_column_name: False}])

    yield connection_string
    with contextlib.suppress(PermissionError):
        # raises 'PermissionError' in Windows
        temp_db_file.unlink()
