# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
import sqlalchemy as sa
from sync_client.io import io_db


def test_get_db_engine(conn_str_to_tmp_db):
    """Test integration with temporary db to create connection and return sa.Engine."""
    engine = io_db.get_db_engine(conn_str_to_tmp_db)
    assert isinstance(engine, sa.Engine)


@pytest.mark.parametrize(
    ("protocol", "protocol_expected", "url_expected"),
    [
        ("mysql", "mysql", r"mysql://username:secret_pw@host_ip:3306/db_name"),
        (
            "postgresql",
            "postgresql+psycopg",
            r"postgresql+psycopg://username:secret_pw@host_ip:5432/db_name",
        ),
        (
            "sqlite",
            "sqlite",
            r"sqlite://username:secret_pw@host_ip/db_name?check_same_thread=False",
        ),
        (
            "oracle",
            "oracle+oracledb",
            r"oracle+oracledb://username:secret_pw@host_ip:1521/db_name",
        ),
        (
            "mssql",
            "mssql+pyodbc",
            r"mssql+pyodbc://username:secret_pw@host_ip:1433/db_name"
            r"?driver=ODBC+Driver+17+for+SQL+Server",
        ),
    ],
)
def test_build_db_connection_string(protocol, protocol_expected, url_expected):
    """
    Check connection spec with valid protocols.

    Check default query of sqllite and mssql as well.
    """
    connection_spec = io_db.build_db_connection_string(
        username="username",
        password="secret_pw",
        database="db_name",
        protocol=protocol,
        host="host_ip",
        port=None,
        query=None,
    )
    url = connection_spec.render_as_string(hide_password=False)
    assert isinstance(connection_spec, sa.URL)
    assert connection_spec.drivername == protocol_expected
    assert url == url_expected


@pytest.mark.parametrize(
    ("database", "url_expected"),
    [
        (
            "relative_path_sqllite.db",
            "sqlite:///relative_path_sqllite.db?check_same_thread=False",
        ),
        (
            "/absolute/path/sqllite.db",
            "sqlite:////absolute/path/sqllite.db?check_same_thread=False",
        ),
        (
            "C:/path/with/letter/sqllite.db",
            "sqlite:///C:/path/with/letter/sqllite.db?check_same_thread=False",
        ),
    ],
)
def test_build_db_connection_string_sqllite(database, url_expected):
    """Check sqllite connection string to start with three leading slashes."""
    connection_spec = io_db.build_db_connection_string(
        protocol="sqlite",
        database=database,
        username=None,
        password=None,
        host=None,
        port=None,
        query=None,
    )
    url = connection_spec.render_as_string(hide_password=False)
    assert url == url_expected


@pytest.mark.parametrize(
    ("query", "query_expected"),
    [
        (None, "ODBC Driver 17 for SQL Server"),
        (
            "driver=ODBC Driver 18 for SQL Server&TrustServerCertificate=Yes",
            "ODBC Driver 18 for SQL Server",
        ),
        (
            "driver=ODBC Driver 17 for SQL Server&TrustServerCertificate=Yes",
            "ODBC Driver 17 for SQL Server",
        ),
    ],
)
def test_build_db_connection_string_mssql(query, query_expected):
    """Check mssql connection string to include a driver query."""
    connection_spec = io_db.build_db_connection_string(
        username="username",
        password="secret_pw",
        database="db_name",
        protocol="mssql",
        host="host_ip",
        port=None,
        query=query,
    )
    assert connection_spec.query["driver"] == query_expected


def test_build_db_connection_string_unsupported():
    """Check error with unsupported protocol."""
    with pytest.raises(ValueError, match="Unsupported protocol: xyz-db"):
        io_db.build_db_connection_string(
            username="username",
            password="secret_pw",
            database="db_name",
            protocol="xyz-db",
            host="host_ip",
            port="80",
            query=None,
        )


def test_build_db_connection_string_port_none():
    """Check port handling, if None."""
    connection_spec = io_db.build_db_connection_string(
        username="username",
        password="secret_pw",
        database="db_name",
        protocol="mssql",
        host="host_ip",
        port=None,
        query=None,
    )
    assert connection_spec.port == 1433


def test_build_db_connection_string_port_empty():
    """Check port handling, if empty string."""
    connection_spec = io_db.build_db_connection_string(
        username="username",
        password="secret_pw",
        database="db_name",
        protocol="mssql",
        host="host_ip",
        port="",
        query=None,
    )
    assert connection_spec.port == 1433


@pytest.mark.parametrize(
    ("query", "query_expected"),
    [
        (
            "TrustServerCertificate=YES&Trusted_connection=No",
            {"TrustServerCertificate": "YES", "Trusted_connection": "No"},
        ),
        ("Trusted_connection=yes", {"Trusted_connection": "yes"}),
        (
            "driver=ODBC Driver 18 for SQL Server"
            "&TrustServerCertificate=Yes&Trusted_connection=No",
            {
                "TrustServerCertificate": "Yes",
                "Trusted_connection": "No",
                "driver": "ODBC Driver 18 for SQL Server",
            },
        ),
    ],
)
def test_parse_query(query, query_expected):
    """Check common query sets."""
    query_parsed = io_db._parse_query(query=query)
    assert len(query_parsed) == len(
        query_expected
    ), "number of query parameter mismatch"
    for key_parsed, value_parsed in query_parsed.items():
        assert key_parsed in query_expected
        assert value_parsed == query_expected[key_parsed]


def test_parse_query_wrong_ampersand():
    """Unused ampersand should raise ValueError."""
    with pytest.raises(ValueError, match="Wrong query format."):
        io_db._parse_query(query="Trusted_connection=yes&")


def test_parse_query_empty():
    """Empty query should return empty dictionary."""
    query_parsed = io_db._parse_query(query=None)
    assert query_parsed == {}
