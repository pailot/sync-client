# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from io import BytesIO

import boto3
import pytest
from moto import mock_s3
from sync_client.config import constants
from sync_client.data_models import DatabaseOperation, DataLocation
from sync_client.io.io_s3 import S3Bucket
from sync_client.sources.s3_trigger import S3TriggerSource

ACCESS_KEY = "access_key"
SECRET_KEY = "secret_key"
BUCKET_NAME = "the_bucket_name"
AWS_REGION = "us-east-1"


@pytest.fixture()
def table_name() -> str:
    return "Schedule"


@pytest.fixture()
def file_name(table_name) -> str:
    return constants.release_folder_prefix + table_name


@pytest.fixture()
def bucket_single_file(file_name) -> S3Bucket:
    with mock_s3():
        s3 = boto3.client("s3", region_name=AWS_REGION)
        s3.create_bucket(Bucket=BUCKET_NAME)
        s3.upload_fileobj(BytesIO(b"My schedule"), BUCKET_NAME, f"{file_name}.parquet")
        yield S3Bucket(ACCESS_KEY, SECRET_KEY, BUCKET_NAME, AWS_REGION)


@pytest.fixture()
def bucket_empty() -> S3Bucket:
    with mock_s3():
        s3 = boto3.client("s3", region_name=AWS_REGION)
        s3.create_bucket(Bucket=BUCKET_NAME)
        yield S3Bucket(ACCESS_KEY, SECRET_KEY, BUCKET_NAME, AWS_REGION)


@pytest.fixture()
def table_names() -> list[str]:
    """Table names. Needed for arrange and assert."""
    return ["TableA", "TableB", "TableC"]


@pytest.fixture()
def file_names(table_names) -> list[str]:
    """File names. Needed for arrange."""
    return [constants.release_folder_prefix + table_name for table_name in table_names]


@pytest.fixture()
def bucket_multiple_files(file_names) -> S3Bucket:
    """Create files in mock bucket."""
    with mock_s3():
        s3 = boto3.client("s3", region_name=AWS_REGION)
        s3.create_bucket(Bucket=BUCKET_NAME)
        for file_name in file_names:
            s3.upload_fileobj(
                BytesIO(b"My schedule"), BUCKET_NAME, f"{file_name}.parquet"
            )
        yield S3Bucket(ACCESS_KEY, SECRET_KEY, BUCKET_NAME, AWS_REGION)


@pytest.fixture()
def subdir_table_names() -> list[str]:
    """Table names. Needed for arrange and assert."""
    return ["schedule/unit_1", "schedule/unit_2"]


@pytest.fixture()
def subdir_file_names(subdir_table_names) -> list[str]:
    """File names with subdirectory. Needed for arrange."""
    return [
        constants.release_folder_prefix + table_name
        for table_name in subdir_table_names
    ]


@pytest.fixture()
def bucket_subdir(subdir_file_names) -> S3Bucket:
    """Create files in mock bucket."""
    with mock_s3():
        s3 = boto3.client("s3", region_name=AWS_REGION)
        s3.create_bucket(Bucket=BUCKET_NAME)
        for file_name in subdir_file_names:
            s3.upload_fileobj(
                BytesIO(b"My schedule"), BUCKET_NAME, f"{file_name}.parquet"
            )
        yield S3Bucket(ACCESS_KEY, SECRET_KEY, BUCKET_NAME, AWS_REGION)


def test_poll_with_single_file(bucket_single_file, table_name):
    """Perform polling, when schedule file is available."""
    s3_trigger_source = S3TriggerSource(
        s3=bucket_single_file,
        table_names=[table_name],
    )

    result = s3_trigger_source.poll()
    expected_data = BytesIO(b"My schedule").getbuffer()

    assert len(result) == 1
    assert result[0].data.getbuffer() == expected_data
    assert result[0].name == table_name
    assert result[0].source == DataLocation.S3
    assert result[0].operation == DatabaseOperation.Replace, "Table has to be replaced"
    assert "Contents" not in bucket_single_file.s3.list_objects(
        Bucket=BUCKET_NAME
    ), "files are not deleted"


def test_poll_with_empty_bucket(bucket_empty, table_name):
    """Perform polling, when no schedule file is available."""
    s3_trigger_source = S3TriggerSource(
        s3=bucket_empty,
        table_names=[table_name],
    )
    result = s3_trigger_source.poll()
    assert result == []


def test_poll_with_multiple_files(bucket_multiple_files, table_names):
    """Perform polling, when schedule files are available."""
    s3_trigger_source = S3TriggerSource(
        s3=bucket_multiple_files,
        table_names=table_names,
    )
    results = s3_trigger_source.poll()
    result = results[0]
    expected_data = BytesIO(b"My schedule").getbuffer()

    assert len(results) == 3
    assert result.data.getbuffer() == expected_data
    assert result.name == table_names[0]
    assert result.source == DataLocation.S3
    assert result.operation == DatabaseOperation.Replace, "Table has to be replaced"

    assert "Contents" not in bucket_multiple_files.s3.list_objects(
        Bucket=BUCKET_NAME
    ), "files are not deleted"


def test_poll_with_multiple_files_missing(
    bucket_multiple_files, file_names, table_names
):
    """Perform polling with missing files."""
    # delete one file
    missing_file = f"{file_names[0]}.parquet"
    bucket_multiple_files.delete_file_from_s3(remote_path=missing_file)

    s3_trigger_source = S3TriggerSource(
        s3=bucket_multiple_files,
        table_names=table_names,
    )
    results = s3_trigger_source.poll()

    assert len(results) == 2
    assert "Contents" not in bucket_multiple_files.s3.list_objects(
        Bucket=BUCKET_NAME
    ), "files are not deleted"


def test_poll_with_partial_tables(bucket_subdir, subdir_table_names):
    """Perform polling, when schedule files are available."""
    s3_trigger_source = S3TriggerSource(
        s3=bucket_subdir,
        table_names=subdir_table_names,
    )
    results = s3_trigger_source.poll()
    result = results[0]
    expected_data = BytesIO(b"My schedule").getbuffer()

    assert len(results) == 2
    assert result.data.getbuffer() == expected_data
    assert result.name == "schedule"
    assert result.source == DataLocation.S3
    assert result.operation == DatabaseOperation.Update, "Table has to be updated"
    assert "Contents" not in bucket_subdir.s3.list_objects(
        Bucket=BUCKET_NAME
    ), "files are not deleted"


def test_poll_with_partial_tables_missing(
    bucket_subdir, subdir_table_names, subdir_file_names
):
    """Perform polling with missing files."""
    # delete one file
    missing_file = f"{subdir_file_names[0]}.parquet"
    bucket_subdir.delete_file_from_s3(remote_path=missing_file)

    s3_trigger_source = S3TriggerSource(
        s3=bucket_subdir,
        table_names=subdir_table_names,
    )
    results = s3_trigger_source.poll()

    assert len(results) == 1
    assert "Contents" not in bucket_subdir.s3.list_objects(
        Bucket=BUCKET_NAME
    ), "files are not deleted"
