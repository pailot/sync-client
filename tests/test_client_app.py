# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from dataclasses import fields
from unittest.mock import MagicMock

import pytest
import sqlalchemy
from sync_client.client_app import SyncClient
from sync_client.config import constants
from sync_client.data_models import DataLocation, DataPayload
from sync_client.io import io_s3
from sync_client.scheduler import RetryTaskScheduler


@pytest.fixture()
def mock_s3():
    return MagicMock(spec=io_s3.S3Bucket)


@pytest.fixture()
def mock_db_watcher():
    return MagicMock(spec=io_s3.S3Bucket)


def create_payload_mock(source):
    mock = MagicMock(spec_set=[field.name for field in fields(DataPayload)])
    mock.source = source
    return mock


@pytest.fixture()
def mock_db_engine():
    return MagicMock(spec=sqlalchemy.Engine)


@pytest.fixture()
def mock_scheduler():
    return MagicMock(spec=RetryTaskScheduler)


@pytest.fixture()
def sync_client(mock_s3, mock_db_engine, mock_scheduler, tmp_path):
    locations_to_upload_to_s3: set[DataLocation] = {DataLocation.LocalFile}
    error_log_path = tmp_path / "error.log"
    error_log_path.write_text("ERROR LOG")
    return SyncClient(
        s3=mock_s3,
        db_engine=mock_db_engine,
        sources=[],
        locations_to_upload_to_s3=locations_to_upload_to_s3,
        scheduler=mock_scheduler,
        error_log_path=error_log_path,
    )


def test_notify_adds_tasks_to_scheduler(sync_client, mock_scheduler):
    mock_payload = create_payload_mock(DataLocation.LocalFile)
    sync_client.notify(mock_payload)

    mock_scheduler.append_to_queue.assert_called_once_with(
        (
            "upload_to_s3",
            {"payload": mock_payload, "prefix": constants.ingest_folder_prefix},
        ),
    )


def test_poll_sources_adds_tasks_to_scheduler(sync_client, mock_scheduler):
    mock_payload = create_payload_mock(DataLocation.LocalFile)
    mock_source = MagicMock()
    mock_source.poll.return_value = [mock_payload]
    sync_client.sources = [mock_source]

    sync_client.poll_sources()

    mock_scheduler.append_to_queue.assert_called_once_with(
        (
            "upload_to_s3",
            {"payload": mock_payload, "prefix": constants.ingest_folder_prefix},
        ),
    )


def test_upload_logs_adds_tasks_to_scheduler(sync_client, mock_scheduler):
    """
    Check if calling the `upload_logs` leads to adding a task to the scheduler.

    Note that: The sync_client adds the tasks it needs to the scheduler.
    """
    sync_client.upload_logs()

    mock_scheduler.append_to_queue.assert_called_once()


def test_run_starts_scheduler(sync_client, mock_scheduler):
    sync_client.run()

    mock_scheduler.run.assert_called_once()
