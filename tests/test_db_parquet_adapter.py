# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from io import BytesIO

import pyarrow as pa
import pyarrow.parquet as pq
import pytest
import sqlalchemy as sa
from sync_client.data_models import DatabaseOperation, DataLocation, DataPayload
from sync_client.io import db_parquet_adapter, io_db


@pytest.fixture()
def database(conn_str_to_tmp_db) -> sa.Engine:
    return io_db.get_db_engine(conn_str_to_tmp_db)


def test_read_all_tables_as_parquet(database, trigger_table_name):
    """Read all tables as parquet."""
    # Test  and assert the results
    results = db_parquet_adapter.read_all_tables_as_parquet_buffer(
        engine=database,
        excluded_tables=[trigger_table_name],
    )
    assert len(results) == 1
    payload = results[0]
    assert payload.name == "data_table.parquet"
    assert isinstance(payload.data, BytesIO)


@pytest.fixture()
def schedule_table_name() -> str:
    return "schedule"


@pytest.fixture()
def replace_payload_1(schedule_table_name) -> DataPayload:
    """Return data payload with four entries."""
    schedule_table_data = [
        {"id": 1, "name": "task_a", "check_column": "a"},
        {"id": 2, "name": "task_b", "check_column": "a"},
        {"id": 3, "name": "task_c", "check_column": "b"},
        {"id": 4, "name": "task_d", "check_column": "b"},
    ]
    data_bytes_io = BytesIO()
    pq.write_table(pa.Table.from_pylist(schedule_table_data), data_bytes_io)
    return DataPayload(
        data=data_bytes_io,
        name=schedule_table_name,
        source=DataLocation.S3,
        operation=DatabaseOperation.Replace,
    )


@pytest.fixture()
def replace_payload_2(schedule_table_name) -> DataPayload:
    """Return data payload with five entries."""
    schedule_table_data = [
        {"id": 1, "name": "task_001", "check_column": "a"},
        {"id": 2, "name": "task_002", "check_column": "a"},
        {"id": 3, "name": "task_101", "check_column": "b"},
        {"id": 4, "name": "task_102", "check_column": "b"},
        {"id": 5, "name": "task_103", "check_column": "b"},
    ]
    data_bytes_io = BytesIO()
    pq.write_table(pa.Table.from_pylist(schedule_table_data), data_bytes_io)
    return DataPayload(
        data=data_bytes_io,
        name=schedule_table_name,
        source=DataLocation.S3,
        operation=DatabaseOperation.Replace,
    )


@pytest.fixture()
def update_payload_filter(schedule_table_name) -> DataPayload:
    """Return data payload, which updates two entries."""
    schedule_table_data = [
        {"id": 3, "name": "task_201", "check_column": "b"},
        {"id": 4, "name": "task_202", "check_column": "b"},
    ]
    data_bytes_io = BytesIO()
    pq.write_table(pa.Table.from_pylist(schedule_table_data), data_bytes_io)
    return DataPayload(
        data=data_bytes_io,
        name=schedule_table_name,
        source=DataLocation.S3,
        operation=DatabaseOperation.Update,
        filter_column="check_column",
        filter_value="b",
    )


@pytest.fixture()
def update_payload_all(schedule_table_name) -> DataPayload:
    """Return data payload, which deletes all rows and adds two rows."""
    schedule_table_data = [
        {"id": 3, "name": "task_201", "check_column": "b"},
        {"id": 4, "name": "task_202", "check_column": "b"},
    ]
    data_bytes_io = BytesIO()
    pq.write_table(pa.Table.from_pylist(schedule_table_data), data_bytes_io)
    return DataPayload(
        data=data_bytes_io,
        name=schedule_table_name,
        source=DataLocation.S3,
        operation=DatabaseOperation.Update,
        filter_column=None,
    )


def test_replace_table(
    database,
    schedule_table_name,
    replace_payload_1,
    replace_payload_2,
):
    """Write to database by replacing existing table."""
    db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=replace_payload_1,
    )
    success_flag = db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=replace_payload_2,
    )
    assert success_flag
    with database.begin() as connection:
        result = connection.execute(
            sa.text(f"SELECT * FROM {schedule_table_name}"),
        ).fetchall()
        assert len(result) == 5
        assert result[0][1] == "task_001", "row has to be replaced"
        assert result[4][1] == "task_103", "row has to be replaced"


def test_update_table_filter(
    database,
    schedule_table_name,
    replace_payload_1,
    update_payload_filter,
):
    """Write to database by replacing filtered rows of given table."""
    db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=replace_payload_1,
    )
    success_flag = db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=update_payload_filter,
    )
    assert success_flag
    with database.begin() as connection:
        result = connection.execute(
            sa.text(f"SELECT * FROM {schedule_table_name}"),
        ).fetchall()
        assert len(result) == 4
        assert result[0][1] == "task_a", "row has to be unchanged"
        assert result[3][1] == "task_202", "row has to be replaced"


def test_update_table_filter_no_table(
    database,
    schedule_table_name,
    replace_payload_1,
    update_payload_filter,
):
    """Write to database first time. Table is not available."""
    success_flag = db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=update_payload_filter,
    )
    assert success_flag


def test_update_table_all(
    database,
    schedule_table_name,
    replace_payload_1,
    update_payload_all,
):
    """Write to database by replacing all rows of given table."""
    db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=replace_payload_1,
    )
    success_flag = db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=update_payload_all,
    )
    assert success_flag
    with database.begin() as connection:
        result = connection.execute(
            sa.text(f"SELECT * FROM {schedule_table_name}"),
        ).fetchall()
        assert len(result) == 2
        assert result[0][1] == "task_201", "row has to be unchanged"
        assert result[1][1] == "task_202", "row has to be replaced"


def test_update_table_all_no_table(
    database,
    schedule_table_name,
    replace_payload_1,
    update_payload_all,
):
    """Write to database first time. Table is not available."""
    success_flag = db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=update_payload_all,
    )
    assert success_flag


def test_write_table_to_db(database):
    """Write new table to database."""
    data = {"id": [1, 2, 3], "name": ["John", "Jane", "Jake"]}
    table_arrow = pa.Table.from_pydict(data)
    table_name = "test_table"
    data_bytes_io = BytesIO()
    pq.write_table(table_arrow, data_bytes_io)

    success_flag = db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=DataPayload(
            data=data_bytes_io, name=table_name, source=DataLocation.S3
        ),
    )
    assert success_flag
    # Verify if the data was written to the database table correctly
    with database.begin() as connection:
        result = connection.execute(
            sa.text(f"SELECT * FROM {table_name}"),
        ).fetchall()
        assert len(result) == len(data["id"])
        for i, row_raw in enumerate(result):
            row = row_raw._asdict()
            assert row["id"] == data["id"][i]
            assert row["name"] == data["name"][i]


def test_write_table_to_db_no_data(database):
    """Check if missing data is handle without error."""
    success_flag = db_parquet_adapter.write_table_to_db(
        engine=database,
        data_payload=DataPayload(data=None, name="test_table", source=DataLocation.S3),
    )
    assert not success_flag


@pytest.mark.parametrize(
    ("pyarrow_dtype", "expected_sa_type", "driver"),
    [
        (pa.bool_(), sa.Boolean, None),
        (pa.int8(), sa.Integer, None),
        (pa.int16(), sa.Integer, None),
        (pa.int32(), sa.Integer, None),
        (pa.int64(), sa.Integer, None),
        (pa.uint8(), sa.Integer, None),
        (pa.uint16(), sa.Integer, None),
        (pa.uint32(), sa.Integer, None),
        (pa.uint64(), sa.Integer, None),
        (pa.float16(), sa.Float, None),
        (pa.float32(), sa.Float, None),
        (pa.float64(), sa.Float, None),
        (pa.decimal128(precision=10, scale=2), sa.Numeric, None),
        (pa.string(), sa.String, None),
        (pa.large_string(), sa.String, None),
        (pa.binary(), sa.LargeBinary, None),
        (pa.large_binary(), sa.LargeBinary, None),
        (pa.date32(), sa.Date, None),
        (pa.date64(), sa.Date, None),
        (pa.timestamp("ns"), sa.DateTime, None),
        (pa.time32("ms"), sa.Time, None),
        (pa.time64("us"), sa.Time, None),
        (pa.null(), sa.Integer, None),
        (pa.duration("s"), sa.Interval, None),
        (pa.duration("ms"), sa.Interval, None),
        (pa.duration("us"), sa.Interval, None),
        (pa.duration("ns"), sa.Interval, None),
        (pa.string(), sa.Text, "oracledb"),
        (pa.time32("ms"), sa.TIMESTAMP, "oracledb"),
    ],
)
def test_get_sqlalchemy_type_from_pyarrow(pyarrow_dtype, expected_sa_type, driver):
    result = db_parquet_adapter._get_sqlalchemy_type_from_pyarrow(
        pyarrow_dtype=pyarrow_dtype,
        driver=driver,
    )
    assert result == expected_sa_type
