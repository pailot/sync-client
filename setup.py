"""
Packaging with cx_freeze is configured with this file.

Dependencies are automatically detected, but explicit defined packages are sometimes
needed to make cx_freeze work.
"""
from cx_Freeze import Executable, setup

build_options = {
    "packages": [
        "os",
        "urllib3",
        "ssl",
        "loguru",
        "sqlalchemy",
        "sqlalchemy.dialects",
        "pyodbc",
        "oracledb",
        "psycopg",
        "boto3",
        "pyarrow",
        "numpy",
        "watchdog",
        "pydantic",
        "cryptography",
    ],
    "excludes": [
        "ruff",
        "moto",
        "pytest",
        "cx-freeze",
        "pytest-asyncio",
        "pytest-cov",
        "black",
    ],
    "include_files": [
        ("config/settings.ini.template", "settings.ini.template"),
        ("README.md", "README.md"),
        ("LICENSE", "LICENSE"),
        ("install_client_as_service.bat", "install_client_as_service.bat"),
        ("remove_client_as_service.bat", "remove_client_as_service.bat"),
        ("nssm", "nssm"),
    ],
}

base = "console"

executables = [
    Executable("src/sync_client/main.py", base=base, target_name="sync-client")
]

setup(
    name="sync-client",
    version="1.7.0",
    description=(
        "Sync-Client - Data Transfer and Synchronization Utility for PAILOT APS"
    ),
    options={"build_exe": build_options},
    executables=executables,
)
