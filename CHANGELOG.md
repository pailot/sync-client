# Changelog Sync-Client

### Instructions Changelog

Code version is incremented with [Semantic Versioning](https://semver.org/).

Template:

```
## x.y.z - yyyy/mm/dd

#### Breaking changes

- Breaking change

#### Feature

- New feature 1
- New feature 2

#### Bugfix, CI-Check, Documentation

- Bugfix 1
- Applied linting, black

```

## 1.7.0 - 2024/06/11


#### Feature

- Updated option `return_tables` to work with partial tables e.g. when working with multiple planning units. Use table names as given and add planning unit IDs as suffix e.g. `PlannedTask/department_a, PlannedTask/department_b`.

---

## 1.6.1 - 2024/03/14

#### Bugfix, CI-Check, Documentation

- Added missing python dependencies to release

---

## 1.6.0 - 2024/03/08

#### Feature

- Added support for Oracle databases with documentation and examples.
- Added option `oracle_path` to configure path to oracle client library. Required only for oracle driver thick mode. Optional entry in settings.ini file.

#### Bugfix, CI-Check, Documentation

- Updated database documentation with example configurations.
- Updated README with description of the new feature.
- Updated python dependencies e.g. parquet library, MSSQL driver, PostgreSQL driver and AWS libraries. 

---


## 1.5.1 - 2024/02/23

#### Bugfix, CI-Check, Documentation

- Added support for missing data types
- Added handling of missing `return_tables`. Logger creates a warning message.

---

## 1.5.0 - 2024/02/15

#### Feature

- Added option `return_tables` to configure a list of released schedule tables. Optional entry in settings.ini file.
- Added support to poll and download multiple files from bucket.

#### Bugfix, CI-Check, Documentation

- Updated database documentation and settings template to be more precise.
- Updated README with description of the new feature.

---

## 1.4.1 - 2024/02/14


#### Bugfix, CI-Check, Documentation

- Moved repo to https://gitlab.com/pailot/sync-client
- Updated company brand to PAILOT GmbH
- Renamed remaining occurrences to "sync-client" or "Sync-Client"

---

## 1.4.0 - 2024/01/24

#### Feature

- Added option `activate_log` to activate logging of console output to file. Optional entry in settings.ini file.
- Trigger time (timestamp when trigger in database is detected) is uploaded to S3 buckets.

#### Bugfix, CI-Check, Documentation

- Updated SQLite documentation and tests.
- Changed date in changelog to yyyy/mm/dd

---

## 1.3.1 - 2024/01/11

#### Bugfix, CI-Check, Documentation

- Fixed database password handling when connecting to database as SQL user
- Added more tests for database interactions.

---

## 1.3.0 - 2023/12/19

#### Feature, Documentation

- Added options to select which file watcher method is used via configuration.
- Added logs to show occurred events.

---

## 1.2.3 - 2023/12/12

#### Bugfix, CI-Check, Documentation

- Added additional events to FileWatcher to detect changes on files

---

## 1.2.2 - 2023/11/08

#### Bugfix, CI-Check, Documentation

- Fixed missing setting to handle a custom certificate bundle
- Added additional documentation for proxy to readme
- Added integration test with proxy and custom certificate bundles

---

## 1.2.1 - 2023/10/24

#### Bugfix, CI-Check, Documentation

- Releases are build automatically for Linux and Windows, when commits are tagged
- Release is created with changelog and download links

---

## 1.2.0 - 2023/10/23

#### Feature

- Added options to set proxy server for connections with 'http' and 'https'

---

## 1.1.0 - 2023/09/13

#### Feature

- Updated Readme with firewall instructions and list of IP ranges.
- Added option to disable parts of the Sync Tool via configuration
- Enabled export of interface database tables using views
- Added region option to internal configuration.
- Added changelog.

---

## 1.0.0 - 2023/09/04

#### Feature

- Initial release for open source under GPLv3
