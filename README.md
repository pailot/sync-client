<!-- Sync-Client: transfer data to and from local files and DB to AWS S3.
Copyright (C) 2023 PAILOT GmbH

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

# Sync-Client - Data Transfer and Synchronization Utility

The Sync-Client is an open-source utility developed by [**PAILOT**](https://www.pailot.com/) to facilitate the seamless transfer of data to and from PAILOT APS when using the PAILOT Bridge service.
This tool allows users to synchronize data from local sources, that is databases and individual files, making the integration of the planning service straightforward.
The Sync-Client is designed to be run on-premise, ensuring data security and control.
This README provides an overview of the tool's features, setup instructions, and configuration options.

### Features

- **Data Source Variety**: The Sync-Client supports two primary data sources:

  - **Source 1: Database (e.g., ERP-tabular data)**: Enables the transfer of data from a interface database to the PAILOT Bridge service.
  - **Source 2: Local Files (e.g., Excel)**: Allows synchronization of local files from a predefined location to the PAILOT Bridge service.
- **Secure Communication**: The tool ensures secure data transfer through only outgoing SSL-encrypted communication. This feature safeguards sensitive information during transmission, and protects the host/on premise system from exposing itself to the public internet.
- **Easy integration**: The tool greatly simplifies the process of exchanging data with the PAILOT Bridge service thus allowing for easy integration of PAILOT APS.

### Synchronization Process

The synchronization process of the Sync-Client can be summarized as follows:

1. **Data Source Detection**: The tool identifies the configured data sources, including the interface database and the local files, based on the provided configuration.
2. **Database Synchronization**: If database synchronization is enabled, the tool connects to the interface database. It monitors the trigger table for changes and detects modifications to the data. When the `active` trigger is set to true, the tool initiates the upload of the entire interface database if the data has changed compared to the last upload.
3. **File Synchronization**: For file synchronization, the tool watches the specified files in the specified folder for any changes. When a new file is added or an existing file is modified, the tool promptly uploads the updated file.
4. **Download released plans**: As soon as the PAILOT Bridge provides a released plan, the Sync-Client downloads this plan and writes it to the `schedule` table in the interface database. This table will always contain the latest released schedule, older versions are overwritten. It is up to you further process the data in this table.

### Secure Communication

Security is a fundamental aspect of the Sync-Client's design.
All communication between the tool and PAILOT Bridge occurs over an SSL-encrypted channel.
This encryption ensures that data remains confidential during transmission, minimizing the risk of unauthorized access or tampering.

---

## Content Readme

1. [Installation](#1-getting-started)
2. [Running the Tool](#2-running-the-tool)
3. [Getting Help](#3-getting-help)
4. [License](#4-license)
5. [Development and Releases](#5-development-and-releases)

---

## 1. Getting Started

## 1.1 Prerequisites

- Supported OS: Windows, Linux
- File synchronisation from local file system or network shares
- Database table synchronisation from interface database
- Database table write to interface database
- [Get the latest release of Sync-Client](https://gitlab.com/pailot/sync-client/-/releases)

## 1.2 Installation

### Windows

- Unzip the packaged Sync-Client into a folder of your choice.
- Follow the instructions in the configuration section.
- Additional resources for missing C++ packages:
  - [Microsoft Visual C++ Redistributable Package](https://cx-freeze.readthedocs.io/en/latest/faq.html#microsoft-visual-c-redistributable-package)
- Setup Sync-Client as Windows Service (see section _Running as a Windows Service_)

### Linux

- Unzip the packaged Sync-Client into a folder of your choice.
- Follow the instructions in the configuration section.

## 1.3 Initial Configuration

To configure the Sync-Client for your environment, follow these steps:

1. **Firewall setup**:

   - Depending on your IT setup you might have to configure you firewall to allow outgoing communication [(See appendix A)](#a-ip-ranges-for-firewall-config).
   - You can also configure the Sync-Client to use a proxy for outgoing communication. See the configuration section for more information.
2. **Setup Interface Database Data Source**: The Sync-Client allows data to be transferred from an interface database, specifically designed for data synchronization. This interface database contains relevant data from ERP/MES systems. It is crucial to create a new and dedicated database for this purpose, separate from the production database. Most standard SQL databases are supported for this setup. To establish this data source, follow these steps:

   - Create the Interface Database: Set up a new database instance exclusively for data synchronization.
     Ensure that Sync-Client has read and write access.
   - Write Data Transfer Scripts: Develop scripts within your ERP/MES system to transfer data to the interface database in the agreed-upon format.
   - Configure Trigger Table: Within the interface database, create a trigger table with a boolean column named `active`.
     This trigger indicates when the data has been updated and should be uploaded.
     Write the trigger appropriately in your update scripts.
3. **Synchronization**: The synchronization process is controlled by the `active` trigger. When the trigger is set to true, the entire interface database is uploaded, and the trigger is reset.
   Note that Sync-Client, will automatically create a table `schedule` where the latest released schedule from PAILOT Bridge is written to. You do not need to create the table.

   - Centralized File Storage: Store the relevant files in a central, local folder accessible to all relevant internal stakeholders.
   - Client Access: Ensure that the Sync-Client has at least read access to the folder containing the files.
   - We recommend maintaining a centralized file location (e.g. network path) for easy access by internal stakeholders, ensuring smooth collaboration during the planning process.
4. **Edit the Configuration**: Open the `settings.ini` file to customize the Sync-Client's behavior. The configuration file is divided into several sections:

   - **File_Sync**: Configure the synchronization of local files.

     - `sync_path`: Provide the absolute path to the folder containing the data files to be synced.
     - `filenames`: List the filenames with extensions of the files to be synced, separated by commas.
   - **Database**: Configure the database connection. See section [2.2 Supported Databases](#22-supported-databases) for configuration examples.

     - `protocol`: Set the database protocol (e.g. `sqlite`).
     - `host`: Specify the IP address of the database host.
     - `port`: Set the database port. If not available, the default ports of the database protocol are used.
     - `database`: Set the database name.
     - `query`: Query parameters for connection string e.g. to specify MSSQL driver. Add multiple values with _'&'_ e.g. _query=TrustServerCertificate=Yes&Trusted_connection=No_.
     - `trigger_table`: Set the table to watch for changes using a trigger. If this value is not set the Database is disabled as a source that will be uploaded.
     - `return_tables`: Comma seperated list with released schedule tables that will be synced
       - "Drop and create table" operations are used when working with a single planning unit. Use table names as given e.g. `PlannedTask, Resource, Station`.
       - "Drop and insert rows" operations are used when working with multiple planning units. Use table names as given and add planning unit IDs as suffix e.g. `PlannedTask/department_a, PlannedTask/department_b`. Only the data from new tables is synced. If the table already contains other data from other units, these are not deleted.
       - Optional entry. If this value is not set the tool will not download a released schedule.
     - `oracle_path`: Required only for oracle driver thick mode. Path to client library e.g. `oracle_path=C:/instantclient_21_13`
   - **Cloud**: Configure cloud storage settings. Proxy settings are optional.

     - `bucket`: Specify the name of the cloud storage bucket to use. This will be provided by PAILOT.
     - `proxy_http`: Set proxy server URL for connections with `http` to be used.
     - `proxy_https`: Set proxy server URL for connections with `https` to be used.
     - `proxy_ca_bundle`: The path to a custom certificate bundle to use when establishing SSL/TLS connections with proxy. Bundle must contain root and intermediate certificates.
     - `proxy_client_cert`: The path to a certificate for proxy TLS client authentication.
     - `proxy_use_forwarding_for_https`: For HTTPS proxies, forward your requests to HTTPS destinations with an absolute URI. We strongly recommend you only use this option with trusted or corporate proxies. Value must be boolean.
     - `activate_log`: Optional entry with value `True`. Activate logging of console output to file.
   - **Secrets**: Configure secrets management.

     - `secrets_path`: Provide the path to the folder containing secret files. Alternatively, secrets can be set via environment variables.
   - **FileWatcher**: Configure which file watcher method is used for synchronization.

     - `file_watch_method`: Specify the name of the method to watch synced files. Three methods are currently available:

       - SystemEvents: Checks after the registration of SystemEvents [Modified, Created, Moved, Closed] whether the hash value of the files to be synchronised has changed compared to the data in the cloud in order to trigger an update in the bucket.
       - Polling: Checks periodically (`watch_interval_sec=1`) by reading the files locally whether a FileEvent has occurred. If an event occurs, the hash values of the files to be synchronised are compared with the data in the cloud to trigger an update in the bucket.
       - BruteForce: Checks periodically (`watch_interval_sec=120`) whether changes exist by comparing the files to be synchronised with the files in the cloud (not recommended).
     - `watch_interval_sec`: Time interval in seconds between checks of the `file_watch_method`. For local checks (such as 'Polling'), we recommend you setting this interval to low values, such as 1. For cloud checks (such as 'BruteForce'), only larger values than 120 are allowed. If 'SystemEvents' is chosen as `file_watch_method`, this field is ignored.
5. **Deactivating Features**: It is possible to disable the different parts of the Sync-Client:

   - File upload: exclude `File_Sync` section in config file.
   - Database upload and schedule download: exclude `Database` section in config file.

     - Database upload: exclude `trigger_table` field in config file.
     - Schedule download: exclude `return_tables` field in config file.
6. **Secrets Management**: If using the `secrets_path` option, create files with the corresponding secret names within the secrets folder.

   - `aws_access_key_id`: AWS bucket access key id.
   - `aws_secret_access_key`: AWS bucket secret access key.
   - `db_username`: Interface database username. Only needed, when database is used and database requires credentials.
   - `db_password`: Interface database password. Only needed, when database is used and database requires credentials.

---

## 2. Running the Tool

The Sync-Client needs to run continuously in the background, e.g. as a Windows service.

### 2.1 Running as a Windows Service

**Automatic Installation**

1. Run `install_client_as_service.bat` with admin privileges to install Sync-Client as Windows service using the [NSSM tool](https://nssm.cc).
2. Open _Service/Microsoft Management Tool_ and start service `PAILOT.SyncClient`.

The Sync-Client will now run as a Windows service, executing the synchronization process based on your configuration.

**Automatic Uninstallation**

1. Run `remove_client_as_service.bat` with admin privileges to remove Sync-Client as Windows service.

**Manual Restart**

Open the Windows service app and search for the service `PAILOT.SyncClient`.
Via context menu the service can be started, stopped or restarted.

### 2.2 Supported Databases

| Version Client | OS      | Database | Tested |
| -------------- | ------- | -------- | ------ |
| 1.7            | Windows | SQLite   | yes    |
| 1.7            | Windows | MSSQL    | yes    |
| 1.7            | Windows | Postgres | no     |
| 1.7            | Windows | Oracle   | yes    |
| 1.7            | Windows | MariaDB  | no     |
| 1.7            | Windows | MySQL    | no     |
| 1.7            | Linux   | SQLite   | yes    |
| 1.7            | Linux   | MSSQL    | no     |
| 1.7            | Linux   | Postgres | no     |
| 1.7            | Linux   | Oracle   | no     |

More database dialects should be functional, but have not been tested.
[See SQLAlchemy supported dialects.](https://docs.sqlalchemy.org/en/20/dialects/)

**MSSQL**

- Protocol: `mssql`
- Default port: `1433`
- Connection string query parameter are set via setting `query`.
  - **Driver:** Specify which ODBC driver to use for communication. Driver needs to be installed on client.
    Most common values are version 17 or 18 e.g. `driver=ODBC Driver 17 for SQL Server`
  - **Encrypt:** ODBC driver version 18 changed default value to `Encrypt=True`, which activates TLS by default
  - **TrustServerCertificate:** Using self-signed certificates could require using `TrustServerCertificate=Yes`, which skips certificate check.
  - **Trusted_connection:** Specifies whether a user connects through a user account or another platform-specific authentication.
    Setting to `Trusted_connection=No` uses credentials from connection string to log in to database.
- Using SQL-User:
  - Set to `query=driver=ODBC Driver 18 for SQL Server&TrustServerCertificate=Yes&Trusted_connection=No`
  - Set secrets `db_username` and `db_password`
- Using Windows-User:
  - Set to `query=driver=ODBC Driver 18 for SQL Server&Trusted_connection=Yes`
  - Do not add secrets `db_username` and `db_password`. Alternative: keep files empty.
- Using self-signed certificates could require using `TrustServerCertificate=Yes`
- Configuration tips:
  - It is not recommended using the `sa` user.
  - Database user has to be assigned to database.
  - Database user needs sufficient privileges for database to read, create and drop tables.
  - Database user default schema should be set according to tables e.g. `dbo`
  - [Download ODBC driver if not installed](https://learn.microsoft.com/en-us/sql/connect/odbc/download-odbc-driver-for-sql-server)
  - [Check available ODBC driver version installed on system](https://learn.microsoft.com/en-us/previous-versions/windows/desktop/odbc/dn170514(v=vs.85))

```ini
# Example MSSQL database settings with SQL-User
[Database]
protocol=mssql
host=127.0.0.1
port=1433
database=mes
query=driver=ODBC Driver 18 for SQL Server&TrustServerCertificate=Yes&Trusted_connection=No
```

**SQLite**

- Protocol: `sqlite`
- Remove settings `host` and `port`
- Path to SQLite file
  - Path has to be defined with setting `database`
  - Absolute paths with Unix: `path/to/database.db`
  - Absolute paths with Windows: `/path/to/database.db` or `C:/path/to/database.db`
- Do not add secrets `db_username` and `db_password`

```ini
# Example SQLite database settings
[Database]
protocol=sqlite
database=/sync_client/my_database.db
```

**PostgreSQL**

- Protocol: `postgresql`
- Default port: `5432`

**Oracle**

- Protocol: `oracle`
- Default port: `1521`
- Thin and Thick mode:
  - Oracle driver runs by default in thin mode, which connects directly to the database.
  - Some features are only supported in thick mode, which requires that Oracle Client Interface (OCI) is installed e.g. [_Instant Client_](https://www.oracle.com/database/technologies/instant-client.html).
  - Feature support differences of thin and thick mode can be found [here](https://python-oracledb.readthedocs.io/en/latest/user_guide/appendix_a.html).
  - Set configuration `oracle_path` with path to client library e.g. `oracle_path=C:/instantclient_21_13`
- Connection string query parameters ([List of available query parameters](https://python-oracledb.readthedocs.io/en/latest/user_guide/appendix_b.html#connection-strings))
  - **Service Name**: Connect to database via `service_name` query parameter
- Configuration tips:
  - It is not recommended using the `system` user, because all system tables would be uploaded.
  - Database user needs sufficient privileges for database to read, create and drop tables.
  - Database user needs sufficient quota for tablespace.
  - Database version support is different between thin and thick mode. See [driver overview](https://python-oracledb.readthedocs.io/en/latest/user_guide/appendix_a.html)
    or [client / server interoperability support matrix](https://support.oracle.com/knowledge/Oracle%20Cloud/207303_1.html) for more information.

```ini
# Example Oracle database settings with service name
[Database]
protocol=oracle
host=127.0.0.1
port=1521
database=
query=service_name=FREEPDB1

# Example Oracle database settings with SID
[Database]
protocol=oracle
host=127.0.0.1
port=1521
database=SID

# Example Oracle database settings with service name and thick mode
[Database]
protocol=oracle
host=127.0.0.1
port=1521
database=
query=service_name=FREEPDB1
oracle_path=C:/instantclient_21_13
```

**MySQL**

- Protocol: `mysql`
- Default port: `3306`

---

## 3. Getting Help

If you encounter any issues or have questions about using the Sync-Client,
feel free to reach out to our [**support team**](mailto:helpdesk@pailot.com).
We value your feedback and contributions to this open-source project.

---

## 4. License

This CLI tool is distributed under the terms of the GNU General Public License version 3 (GPLv3).

### GNU General Public License (GPLv3)

The full text of the GPLv3 license is available in the [LICENSE](LICENSE) file in the root directory of this project.

#### Permissions

- You are free to use, modify, and distribute this software in accordance with the terms of the GPLv3.

#### Obligations

- If you make modifications to this software, you must share those modifications under the same GPLv3 license.
- When distributing this software or any modifications, you must provide access to the corresponding source code.

### Contributions

Contributions to this project are welcome, but contributors must agree to license their contributions under the terms of the GPLv3.

For more details, please refer to the [LICENSE](LICENSE) file.

For questions or licensing inquiries, contact [**PAILOT GmbH**](mailto:helpdesk@pailot.com).

---

## 5. Development and Releases

**Prepare a new release**

- Add new version number and changes to [CHANGELOG.md](https://gitlab.com/pailot/sync-client/-/blob/main/CHANGELOG.md)
- Updated version numbers in `setup.py` and `pyproject.toml`

**Build a new release**

- Sync-Client is packaged with `cx_freeze`.
- Tag commit with new version number, which triggers CI pipeline to build a new release.

**Latest release**

One can find the latest release and a list of all releases [here](https://gitlab.com/pailot/sync-client/-/releases)

**Optional: Custom certificate bundle**

When establishing SSL/TLS connections with proxy, a path to a custom certificate bundle could be needed.
This bundle must contain root and intermediate certificates of your custom certificate chain as well
a bundle of CA Root certificates to authorize with AWS servers.

One can create a custom bundle with following steps:

1. Get latest CA certificates as PEM file from your source of trust
   - a) Certificates based on Mozilla CA certificate store from [Curl](https://curl.se/docs/caextract.html) ([cacert.pem](https://curl.se/ca/cacert.pem))
   - b) Certificates based on Amazon certification chain from [S3 bucket endpoint URL](https://randonname.s3.eu-central-1.amazonaws.com/)
     (get certificate chain via browser from endpoint)
   - c) Your company certificate bundle
   - Files with extension .pem usually have .crt extension in Windows OS.
   - Both file extensions can be used, as long certificates are base64 encoded
2. Get your latest custom certificate, which you want to include in the bundle
   - Windows: Certificate Manager > Export Certificate > base64 encoded
3. Combine files to a single certificate bundle
   - Manual: Copy your certificate at the end of the CA certificate bundle
   - Powershell: `type your_certificate.crt, cacert.pem > custom_cert_bundle.pem`
   - Unix: `cat your_certificate.pem cacert.pem > custom_cert_bundle.pem`
4. Certificates have a due date and need to be renewed on a regular basis. Usually once a year.

---

# Appendix:

## A: IP ranges for firewall config

Depending on your IT setup you might have to configure you firewall to all outgoing communication to the following IP ranges.
Allowing TCP over port 443 for HTTPS should be sufficient.

| ip_prefix       | region       |
| :-------------- | :----------- |
| 13.248.224.0/24 | GLOBAL       |
| 13.248.225.0/24 | GLOBAL       |
| 13.248.226.0/24 | GLOBAL       |
| 13.248.227.0/24 | GLOBAL       |
| 13.248.228.0/24 | GLOBAL       |
| 13.248.229.0/24 | GLOBAL       |
| 13.248.230.0/24 | GLOBAL       |
| 13.248.231.0/24 | GLOBAL       |
| 13.248.232.0/24 | GLOBAL       |
| 13.248.233.0/24 | GLOBAL       |
| 76.223.100.0/24 | GLOBAL       |
| 76.223.101.0/24 | GLOBAL       |
| 76.223.102.0/24 | GLOBAL       |
| 76.223.103.0/24 | GLOBAL       |
| 76.223.104.0/24 | GLOBAL       |
| 76.223.95.0/24  | GLOBAL       |
| 76.223.96.0/24  | GLOBAL       |
| 76.223.97.0/24  | GLOBAL       |
| 76.223.98.0/24  | GLOBAL       |
| 76.223.99.0/24  | GLOBAL       |
| 16.12.24.0/21   | eu-central-1 |
| 16.12.32.0/22   | eu-central-1 |
| 3.5.134.0/23    | eu-central-1 |
| 3.5.136.0/22    | eu-central-1 |
| 3.65.246.0/28   | eu-central-1 |
| 3.65.246.16/28  | eu-central-1 |
| 52.219.140.0/24 | eu-central-1 |
| 52.219.168.0/24 | eu-central-1 |
| 52.219.169.0/24 | eu-central-1 |
| 52.219.170.0/23 | eu-central-1 |
| 52.219.208.0/23 | eu-central-1 |
| 52.219.210.0/24 | eu-central-1 |
| 52.219.211.0/24 | eu-central-1 |
| 52.219.218.0/24 | eu-central-1 |
| 52.219.44.0/22  | eu-central-1 |
| 52.219.72.0/22  | eu-central-1 |
| 16.62.56.224/28 | eu-central-2 |
| 16.62.56.240/28 | eu-central-2 |
| 3.5.52.0/22     | eu-central-2 |
| 52.95.139.0/24  | eu-central-2 |
| 52.95.140.0/23  | eu-central-2 |

_Disclaimer: This document is intended for informational purposes only. All product and company names mentioned are trademarks™ or registered® trademarks of their respective holders. Use of these trademarks does not imply any affiliation with or endorsement by the respective holders._
