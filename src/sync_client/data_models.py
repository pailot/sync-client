# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""The local side settings for the Sync-Client."""
from dataclasses import dataclass
from enum import IntEnum
from io import BytesIO
from pathlib import Path

from sync_client.config import constants


def read_file_to_buffer(file_path: Path) -> BytesIO:
    buffer = BytesIO(file_path.read_bytes())
    buffer.seek(0)
    return buffer


class DataLocation(IntEnum):
    """Enumeration representing different data storage locations."""

    S3 = 0
    Database = 1
    LocalFile = 2


class DatabaseOperation(IntEnum):
    """Enumeration representing different database operations."""

    # delete table & create new table
    Replace = 0
    # delete rows & insert new rows
    Update = 1


@dataclass
class DataPayload:
    """
    Represents a data payload with its associated metadata.

    If 'operation' is 'DatabaseOperation.Update', use 'filter_*' to define
    update behaviour.

    filter_column:
    - None: drop all rows and insert new rows
    - String: Query column name and update where row equals 'filter_value'.

    filter_value:
    - only used if  'filter_column' is not None.
    """

    data: BytesIO | None
    name: str
    source: DataLocation
    operation: DatabaseOperation = DatabaseOperation.Replace
    filter_column: str | None = None
    filter_value: str | None = None

    @classmethod
    def from_file(cls, path: Path | str, name=None):
        data = read_file_to_buffer(Path(path))
        remote_name = name or path.name
        return DataPayload(data, remote_name, DataLocation.LocalFile)


@dataclass
class S3File:
    """
    Data to handle S3 bucket files for download and trigger.

    example 'schedule': table 'schedule', no planning_unit.
    example 'schedule/unit_a': table 'schedule', planning_unit 'unit_a'

    Args:
    ----
        table_name: Config value from Database.return_tables

    """

    table_name: str
    file_name: str
    is_directory: bool
    directory_name: str | None

    def __init__(self, table_name: str):
        """Convert table name from config."""
        self.table_name = self._convert_to_table_name(table_name=table_name)
        self.file_name = self._convert_to_file_name(table_name=table_name)
        self.is_directory = self._is_directory(table_name=table_name)
        self.planning_unit = self._convert_to_planning_unit_name(table_name=table_name)

    def _is_directory(self, table_name: str) -> bool:
        """Check for '/' (slash) in table name."""
        return constants.directory_character in table_name

    def _convert_to_file_name(self, table_name: str) -> str:
        """Convert table name to S3 bucket file name."""
        return (
            f"{constants.release_folder_prefix}"
            f"{table_name}"
            f"{constants.table_file_extension}"
        )

    def _convert_to_table_name(self, table_name: str) -> str:
        """Convert to table name without directory substring."""
        if self._is_directory(table_name=table_name):
            return table_name.split(constants.directory_character)[0]
        return table_name

    def _convert_to_planning_unit_name(self, table_name: str) -> str | None:
        """Convert to directory name without table substring."""
        if self._is_directory(table_name=table_name):
            return table_name.split(constants.directory_character)[1]
        return None
