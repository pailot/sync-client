# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Read and validate the settings from an INI file."""

import configparser
import logging
import sys
from pathlib import Path
from typing import Literal

from loguru import logger
from pydantic import (
    BaseModel,
    BaseSettings,
    Extra,
    ValidationError,
    root_validator,
    validator,
)
from watchdog.observers.api import DEFAULT_OBSERVER_TIMEOUT

from sync_client.config import constants
from sync_client.sources.brute_force_observer import DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT


class ClientSettings(BaseModel):
    class Config:  # noqa: D106
        extra = Extra.forbid


class FileSyncConfig(ClientSettings):
    sync_path: Path
    filenames: list[str]

    @validator("filenames", pre=True)
    def split_filenames(cls, value):  # noqa: N805
        if isinstance(value, str):
            return [s.strip() for s in value.split(",")]
        return value


class DBConfig(ClientSettings):
    protocol: str
    database: str
    host: str | None
    port: str | None = None
    trigger_table: str | None
    query: str | None = None
    return_tables: list[str] | None = None
    oracle_path: str | None = None

    @validator("return_tables", pre=True)
    def split_return_tables(cls, value):  # noqa: N805
        if isinstance(value, str):
            return [s.strip() for s in value.split(",")]
        return value


class CloudConfig(ClientSettings):
    bucket: str
    # merged config values to pass to S3Bucket.
    proxy: dict | None
    proxy_config: dict | None
    # values for config
    proxy_http: str | None
    proxy_https: str | None
    proxy_ca_bundle: str | None
    proxy_client_cert: str | None
    proxy_use_forwarding_for_https: bool | None
    # optional success log
    activate_log: bool | None


class SecretsConfig(ClientSettings):
    secrets_path: Path | None = None


class FileWatcherConfig(ClientSettings):
    """Possible watchers with their corresponding polling interval."""

    file_watch_method: Literal["SystemEvents", "Polling", "BruteForce"] = "SystemEvents"
    # TODO: use validator and take the observer list from file_watchers
    watch_interval_sec: int | None

    @root_validator
    def check_possible_values(cls, values):  # noqa: N805
        file_watch_method = values.get("file_watch_method")
        watch_interval_sec = values.get("watch_interval_sec")

        if file_watch_method == "SystemEvents" and watch_interval_sec is not None:
            logger.warning(
                "'watch_interval_sec' is defined with"
                f" {watch_interval_sec}, but it is ignored because"
                f" 'file_watch_method' is {file_watch_method}."
            )
            values["watch_interval_sec"] = None

        elif file_watch_method == "Polling" and watch_interval_sec is None:
            logger.warning(
                "'watch_interval_sec' is not defined."
                f" The default value ({DEFAULT_OBSERVER_TIMEOUT})"
                f" for selected method: {file_watch_method} is used."
            )
            values["watch_interval_sec"] = DEFAULT_OBSERVER_TIMEOUT

        elif file_watch_method == "Polling" and watch_interval_sec == 0:
            logger.warning(
                f"'watch_interval_sec' is defined as {watch_interval_sec},"
                f" which is not allowed. Therefore,"
                f" the default value ({DEFAULT_OBSERVER_TIMEOUT})"
                f" for the selected file_watch_method {file_watch_method} is used."
            )
            values["watch_interval_sec"] = DEFAULT_OBSERVER_TIMEOUT

        elif file_watch_method == "BruteForce" and watch_interval_sec is None:
            logger.warning(
                "'watch_interval_sec' is not defined."
                f" The default value ({DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT})"
                f" for selected method: {file_watch_method} is used."
            )
            values["watch_interval_sec"] = DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT

        elif (
            file_watch_method == "BruteForce"
            and watch_interval_sec < DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT
        ):
            logger.warning(
                "'watch_interval_sec' is defined with a lower value than permitted"
                f" ({watch_interval_sec} < {DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT})"
                f" for selected method: {file_watch_method}. Therefore,"
                f" the default value ({DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT}) is used."
            )
            values["watch_interval_sec"] = DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT

        return values


class CredentialsSettings(BaseSettings):
    aws_access_key_id: str
    aws_secret_access_key: str
    db_username: str | None
    db_password: str | None

    class Config:  # noqa: D106
        secrets_dir = ""


class AppConfigFromINI(ClientSettings):
    """A limited model to disallow setting credentials in the INI."""

    File_Sync: FileSyncConfig | None = None
    Database: DBConfig | None = None
    Cloud: CloudConfig
    Secrets: SecretsConfig | None = None
    FileWatcher: FileWatcherConfig = FileWatcherConfig()

    @root_validator
    def at_least_one_source(cls, values):  # noqa: N805
        if values.get("File_Sync") is None and values.get("Database") is None:
            msg = (
                "At least one data source must be configured: a database or a sync file"
                " directory."
            )
            raise ValidationError(msg)
        return values


class AppConfig(AppConfigFromINI):
    Credentials: CredentialsSettings
    error_log_path: Path
    success_log_path: Path | None


def _get_proxy_definitions(cloud: dict) -> dict | None:
    """Merge config fields into a dictionary."""
    proxy_definitions = {
        key: cloud[config_key]
        for key, config_key in [("http", "proxy_http"), ("https", "proxy_https")]
        if cloud[config_key] is not None
    }

    if proxy_definitions:
        return proxy_definitions
    return None


def _get_proxy_config(cloud: dict) -> dict | None:
    """Merge config fields into a dictionary."""
    proxy_config = {
        config_key: cloud[config_key]
        for config_key in [
            "proxy_ca_bundle",
            "proxy_client_cert",
            "proxy_use_forwarding_for_https",
        ]
        if cloud[config_key] is not None
    }

    if proxy_config:
        return proxy_config
    return None


def read_config_from_ini(file_path):
    """
    Read config from settings.ini and add additional values.

    Adding settings for:
    - Credentials
    - Logging
    - Proxy
    """
    parser = configparser.ConfigParser()
    parser.read(file_path)

    config_data = {}
    for section_name in parser.sections():
        config_data[section_name] = dict(parser[section_name])

    try:
        config_from_ini = AppConfigFromINI(**config_data)
        config_args = config_from_ini.dict()

        # Credentials
        secrets_path = config_from_ini.Secrets.secrets_path
        config_args["Credentials"] = CredentialsSettings(
            _secrets_dir=secrets_path
        ).dict()

        # Constants
        config_args["error_log_path"] = Path.cwd() / constants.local_error_log
        if config_from_ini.Cloud.activate_log:
            config_args["success_log_path"] = Path.cwd() / constants.local_success_log
        else:
            config_args["success_log_path"] = None

        # Proxy
        config_args["Cloud"]["proxy"] = _get_proxy_definitions(
            cloud=config_args["Cloud"]
        )
        config_args["Cloud"]["proxy_config"] = _get_proxy_config(
            cloud=config_args["Cloud"]
        )
        config = AppConfig.parse_obj(config_args)

    except ValidationError as e:
        errors = "\n" + "\n".join(
            [f"{':'.join(error['loc'])} -> {error['msg']}" for error in e.errors()],
        )
        msg = f"INI file validation errors: {errors}"
        logging.error(msg)
        sys.exit(1)

    return config


def find_settings_file():
    script_directory = Path(__file__).parents[3]

    """Finds the settings.ini file in either the 'config'
      directory or the main directory of the tool"""
    config_path = script_directory / "config" / "settings.ini"
    if config_path.exists():
        return config_path

    fallback_path = script_directory / "settings.ini"
    if fallback_path.exists():
        return fallback_path

    return None


def get_settings() -> AppConfig:
    ini_file_path = find_settings_file()
    if ini_file_path is None:
        logger.error(
            "No settings file found."
            " Place settings.ini in the main directory of the tool."
        )
        sys.exit(1)

    logger.info(f"Loading INI from {ini_file_path}")
    return read_config_from_ini(ini_file_path)
