# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Configuration settings not configurable by the user."""


# Settings for the DB trigger table
trigger_column = "active"
trigger_time_log = "trigger_time.log"

# Protocol specific driver and ports
default_ports = {
    "sqlite": None,
    "mssql": "1433",
    "oracle": "1521",
    "postgresql": "5432",
    "mysql": "3306",
}
protocol_driver = {
    "sqlite": "sqlite",
    "mssql": "mssql+pyodbc",
    "oracle": "oracle+oracledb",
    "postgresql": "postgresql+psycopg",
    "mysql": "mysql",
}

# Error log file paths
remote_error_log = "error.log"
local_error_log = "sync_client_logs/error.log"
local_success_log = "sync_client_logs/success.log"

# s3 config
ingest_folder_prefix = "ingest/"
release_folder_prefix = "release/"
directory_character = r"/"
table_file_extension = ".parquet"
aws_region = "eu-central-1"
