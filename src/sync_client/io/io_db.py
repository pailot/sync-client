# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Database interaction."""
import sqlalchemy as sa
from loguru import logger

from sync_client.config import constants
from sync_client.config.app_config import AppConfig


def get_db_engine(connection_spec: AppConfig | str) -> sa.Engine:
    """Get sqlalchemy database engine from connection string or app configuration."""
    engine_options = {}
    if isinstance(connection_spec, AppConfig):
        connection_string = build_db_connection_string(
            username=connection_spec.Credentials.db_username,
            password=connection_spec.Credentials.db_password,
            database=connection_spec.Database.database,
            protocol=connection_spec.Database.protocol,
            host=connection_spec.Database.host,
            port=connection_spec.Database.port,
            query=connection_spec.Database.query,
        )
        if connection_spec.Database.oracle_path is not None:
            engine_options = {
                "thick_mode": {"lib_dir": connection_spec.Database.oracle_path}
            }
    else:
        connection_string = connection_spec

    engine = sa.create_engine(
        connection_string,
        pool_size=5,
        pool_pre_ping=True,
        **engine_options,
    )

    logger.info(f"Test DB connection {engine.url}...")
    with engine.connect():
        pass
    logger.success(
        f"Created DB connection ({engine.url.drivername}) for:"
        f" {engine.url.host or engine.url}:{engine.url.port or ''}"
    )
    return engine


def build_db_connection_string(  # noqa: PLR0913
    username,
    password,
    database,
    protocol,
    host,
    port=None,
    query=None,
) -> sa.URL:
    """Build connection string based on settings.ini and constants."""
    if protocol not in constants.protocol_driver:
        message = f"Unsupported protocol: {protocol}"
        raise ValueError(message)

    driver = constants.protocol_driver[protocol]
    if port is None or port == "":
        port = constants.default_ports[protocol]

    query_parsed = _parse_query(query=query)
    if protocol == "sqlite":
        query_parsed.update({"check_same_thread": "False"})
    elif protocol == "mssql" and (query_parsed.get("driver", None) is None):
        query_parsed.update({"driver": "ODBC Driver 17 for SQL Server"})

    return sa.URL.create(
        drivername=driver,
        username=username,
        password=password,
        host=host,
        port=port,
        database=database,
        query=query_parsed,
    )


def _parse_query(query: str) -> dict:
    """
    Parse query from settings.ini to dictionary.

    Input: "TrustServerCertificate=YES&Trusted_connection=No"
    Output: {"TrustServerCertificate": "YES", "Trusted_connection": "No"}.
    """
    query_parsed = {}
    if query is None:
        return query_parsed
    for entry in query.split("&"):
        if "=" not in entry:
            msg = f"Wrong query format. {entry}"
            raise ValueError(msg)
        key, value = entry.split("=")
        query_parsed[key.strip()] = value.strip()

    return query_parsed
