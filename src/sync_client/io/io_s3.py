# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""S3 integration."""
import hashlib
from copy import copy
from io import BytesIO

import boto3
import botocore
from loguru import logger

from sync_client.exceptions import S3ValidationError


def calculate_md5(buffer: BytesIO) -> str:
    """Calculate the MD5 checksum of a buffer."""
    buffer.seek(0)
    md5 = hashlib.md5()  # noqa: S324
    for chunk in iter(lambda: buffer.read(4096), b""):
        md5.update(chunk)
    buffer.seek(0)
    return md5.hexdigest()


class S3Bucket:
    """
    A class for interacting with an Amazon S3 bucket.

    This class provides methods to connect to an S3 bucket using the provided
    access key and secret key, upload files to the bucket, download files from
    the bucket, and delete files from the bucket.

    """

    def __init__(  # noqa: PLR0913
        self,
        access_key: str,
        secret_key: str,
        bucket_name: str,
        aws_region_name: str,
        proxy_definitions: dict | None = None,
        proxies_config: dict | None = None,
    ):
        self.access_key = access_key
        self.secret_key = secret_key
        self.bucket = bucket_name
        config = botocore.config.Config(
            region_name=aws_region_name,
            proxies=proxy_definitions,
            proxies_config=proxies_config,
        )
        verify = None
        if proxies_config and proxies_config.get("proxy_ca_bundle", None):
            verify = proxies_config.get("proxy_ca_bundle")

        self.s3 = boto3.client(
            "s3",
            aws_access_key_id=self.access_key,
            aws_secret_access_key=self.secret_key,
            region_name=aws_region_name,
            config=config,
            verify=verify,
        )
        self.connect_to_bucket()

    def connect_to_bucket(self):
        try:
            self.s3.head_bucket(Bucket=self.bucket)
            logger.success(f"Connection to {self.bucket} bucket successful.")
        except botocore.exceptions.SSLError as error:
            logger.error(f"Cannot connect to {self.bucket}.", error)
            logger.error(
                "Please check your certificate bundle to "
                "include all needed certificates."
            )
        except Exception as error:  # noqa: BLE001
            logger.error(f"Cannot connect to {self.bucket}.", error)

    def upload_to_s3(self, buffer: BytesIO, remote_path: str) -> bool:
        """Upload a local file to S3 if it's different from the file already present."""
        if self._are_equal_local_and_remote(remote_path, buffer):
            logger.info(
                f"File local data is already present on S3 {remote_path} and has the"
                " same content.",
            )
            return False

        buffer_copy = copy(buffer)  # upload_fileobj closes the buffer
        # bug in boto3 https://github.com/boto/s3transfer/issues/80
        self.s3.upload_fileobj(buffer_copy, self.bucket, remote_path)

        if not self._are_equal_local_and_remote(remote_path, buffer):
            logger.error(f"Upload failed files are not equal {remote_path}.")
            raise S3ValidationError

        logger.success(f"Uploaded file to: {remote_path}")
        return True

    def _are_equal_local_and_remote(self, remote, local: BytesIO):
        if local.closed:
            msg = f"The buffer is closed: {local!r}"
            raise OSError(msg)

        local_data_md5 = calculate_md5(local)

        try:
            s3_object_info = self.s3.head_object(Bucket=self.bucket, Key=remote)
            s3_etag = s3_object_info["ETag"].strip('"')  # Get the ETag (MD5 checksum)

            if local_data_md5 == s3_etag:
                return True

        except botocore.exceptions.ClientError as error:
            if error.response["Error"]["Code"] == "404":
                logger.debug("File does not exist")
                return False
            raise

        return False

    def download_from_s3(self, remote_path: str) -> None | BytesIO:
        try:
            buffer = self._read_to_buffer(remote_path)

            if not self._are_equal_local_and_remote(remote_path, buffer):
                msg = f"Download failed. Files are not equal {remote_path}"
                raise S3ValidationError(  # noqa: TRY301, RUF100
                    msg,
                )

        except botocore.exceptions.ClientError as error:
            if error.response["Error"]["Code"] == "404":
                # Ignore File not found errors
                return None
            raise
        else:
            return buffer

    def delete_file_from_s3(self, remote_path: str):
        self.s3.delete_object(Bucket=self.bucket, Key=remote_path)

    def _read_to_buffer(self, file_name) -> BytesIO:
        buffer = BytesIO()
        self.s3.download_fileobj(self.bucket, file_name, buffer)
        buffer.seek(0)
        return buffer

    def is_file_available(self, remote_path: str) -> bool:
        """Check if file is available via head request."""
        try:
            self.s3.head_object(Bucket=self.bucket, Key=remote_path)
        except botocore.exceptions.ClientError as exception:
            if exception.response["Error"]["Code"] == "404":
                return False
            raise
        return True
