# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""A scheduler for the Sync-Client using rocketry."""
import asyncio

import rocketry
from loguru import logger
from rocketry import Rocketry
from rocketry.conds import every


class RetryTaskScheduler:
    """
    A scheduler for managing and executing tasks with retry functionality.

    This class provides a mechanism for scheduling and executing tasks, along with
    the ability to automatically retry failed tasks up to a certain number of times.
    It utilizes the Rocketry library for scheduling and execution.

    Tasks have be added first with the `add_tasks` method.

    """

    def __init__(
        self,
        max_retries=4,
    ):
        self.task_queue = []
        self.max_retries = max_retries

        rocketry = Rocketry(
            config={
                "silence_task_prerun": False,
                "silence_task_logging": False,
                "silence_cond_check": False,
            },
        )

        # To refresh the memory as task log are kept in memory
        rocketry.session.create_task(
            func=self.clear_scheduler_logs,
            start_cond=every("10 second"),
            execution="async",
        )

        rocketry.session.create_task(
            func=self.run_task_in_queue,
            name="task_queue_runner",
            start_cond=every("0.3 seconds"),
            execution="async",
        )

        self.rocketry = rocketry

    def add_tasks(
        self,
        tasks: list[tuple[callable, str | None]],
    ):
        """
        Add a list of task specification to the scheduler.

        Each task is a tuple of a callable and a scheduling string or None.
        e.g. (upload_logs, 'every 5 seconds')
        The string syntax has to follow the rocketry conventions
        https://rocketry.readthedocs.io/en/stable/tutorial/basic.html

        if None is passed for scheduling, the task will only be executed if
        added to the queue `append_to_queue`
        """
        for func, timing in tasks:
            self.rocketry.session.create_task(
                func=func,
                start_cond=timing,
            )

    def append_to_queue(self, task: tuple[str, dict]):
        """
        Add a task to the queue for ASAP execution.

        The tasks are added to the task queue and executed ASAP. If a task fails
        it will be retried `self.max_retries` times.
        each task has the tuple format (<callable_name>,<kwargs_dict for the callable>)
        e.g. ("upload_to_s3", {"payload": payload})

        The callables have to be added first via the `add_tasks` method
        """
        self.task_queue.append(task)

    async def run_task_in_queue(self):
        """Execute tasks from the queue with retry and failure handling."""
        if not self.task_queue:
            return

        task_name, params = self.task_queue.pop(0)
        task = self.rocketry.session[task_name]
        n_retries = params.pop("n_retries", 0)
        await asyncio.sleep(n_retries * 2)

        task.run(**params)
        # give the event loop time to start the task
        await asyncio.sleep(0.2)

        while task.is_running:
            await asyncio.sleep(0.2)
        if task.get_status() != "success":
            if n_retries < self.max_retries:
                params["n_retries"] = n_retries + 1
                logger.error(
                    f"Error executing task {task_name}. {params} Retry:"
                    f" #{n_retries + 1}"
                )
                self.task_queue.append((task_name, params))
            else:
                logger.critical(f"Could not execute task {task_name} {params}")

    def run(self):
        """Start executing tasks according to the defined schedule."""
        self.rocketry.run()

    async def clear_scheduler_logs(self):
        """
        Delete all log entries for all task to avoid memory leaks.

        Remove logged messages from rocketry logger
        This is necessary since rocketry uses an in memory red bird repository for
        storing its logs. This results in a ever-increasing memory consumption of
        at least 5 MB / hour.
        There is an issue for Rocketry that tries to solve this:
        https://github.com/Miksus/rocketry/issues/142
        """
        repo = self.rocketry._get_task_logger().handlers[0].repo  # noqa: SLF001
        task: rocketry.core.Task
        for task in self.rocketry.session.get_tasks():
            repo.filter_by(task_name=task.name).delete()
