# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Create a modified observer with a time-driven trigger."""

import threading
from pathlib import Path

from watchdog.events import DirDeletedEvent, FileModifiedEvent
from watchdog.observers.api import (
    BaseObserver,
    EventEmitter,
)

DEFAULT_BRUTEFORCE_EMITTER_TIMEOUT = 120  # in seconds.
DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT = 120  # in seconds.


class BruteForceEmitter(EventEmitter):
    """
    Platform-independent emitter.

    Emitter for a time-driven trigger of change events of a directory.

    NOTE: This Emitter does not detect any changes. It delegates that to the Cloud.
    """

    def __init__(
        self,
        event_queue,
        watch,
        timeout=DEFAULT_BRUTEFORCE_EMITTER_TIMEOUT,
    ):
        super().__init__(event_queue, watch, timeout)
        self._lock = threading.Lock()
        self._watch_path = Path(watch.path)

    def queue_events(self, timeout):
        # We don't want to hit the disk continuously.
        # timeout behaves like an interval for polling emitters.
        if self.stopped_event.wait(timeout):
            return

        with self._lock:
            if not self.should_keep_running():
                return

            try:
                self._watch_path.exists()
            except OSError:
                self.queue_event(DirDeletedEvent(self.watch.path))
                self.stop()
                return

            for file in self._watch_path.glob("*"):
                if file.is_file:
                    self.queue_event(FileModifiedEvent(file))


class BruteForceObserver(BaseObserver):
    """
    Platform-independent observer.

    Observer for a time-driven trigger of change events of a directory.

    NOTE: This Observer does not detect any changes. It delegates that to the Cloud.
    """

    def __init__(self, timeout=DEFAULT_BRUTEFORCE_OBSERVER_TIMEOUT):
        super().__init__(emitter_class=BruteForceEmitter, timeout=timeout)
