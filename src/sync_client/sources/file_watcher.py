# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Define file_watcher based on available observers for a directory."""

from watchdog.observers import Observer
from watchdog.observers.polling import PollingObserver

from sync_client.config.app_config import FileWatcherConfig
from sync_client.sources.brute_force_observer import BruteForceObserver

OBSERVER_MAP = {
    "SystemEvents": Observer,
    "Polling": PollingObserver,
    "BruteForce": BruteForceObserver,
}


def get_observer(settings: FileWatcherConfig) -> Observer:
    """Create an observer instance for file monitoring."""
    observer = OBSERVER_MAP[settings.file_watch_method]

    if settings.watch_interval_sec is not None:
        return observer(timeout=settings.watch_interval_sec)

    return observer()
