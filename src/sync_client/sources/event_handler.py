# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Create an event_handler to select which events should be watched."""

import contextlib
import time
from pathlib import Path

from loguru import logger
from watchdog.events import FileSystemEvent, FileSystemEventHandler

from sync_client.config.app_config import FileSyncConfig
from sync_client.data_models import DataPayload
from sync_client.exceptions import FileUnstableError


class ModifiedFileHandler(FileSystemEventHandler):
    """
    Handles modifications in files in a directory and notify a listener of changes.

    Args:
    ----
        directory_to_handle: The path of the directory to be handled.
        included_filenames: A set of filenames to consider for monitoring.
        listener: List of objects implementing the listener interface to notify.
        max_sec_wait_for_stable_file: Maximum time (in seconds) to wait
          for a file to stabilize after modification, before triggering
          a changed file event.

    """

    def __init__(
        self,
        *,
        directory_to_handle: str,
        included_filenames: set[str],
        listener: list | None,  # todo define interface
        max_sec_wait_for_stable_file=15,
    ):
        self.directory_to_handle = Path(directory_to_handle)
        self.included_filenames = set(included_filenames)
        self.listener = listener or []
        self.max_sec_wait_for_stable_file = max_sec_wait_for_stable_file

        self.notify_all_initially_present_files()

    def on_modified(self, event: FileSystemEvent):
        """File is modified check if it can be uploaded."""
        self.notify(Path(event.src_path), event.event_type)

    def on_created(self, event: FileSystemEvent):
        """File is newly created check if it can be uploaded."""
        self.notify(Path(event.src_path), event.event_type)

    def on_moved(self, event: FileSystemEvent):
        """File is moved or renamed, check if it can be uploaded."""
        path = Path(event.src_path)
        if path.exists():
            self.notify(path, event.event_type)
        else:
            logger.info(
                f"File event: {event.event_type} → {path}."
                f" {path} is not uploaded"
                " because it was moved."
            )

    def on_closed(self, event: FileSystemEvent):
        """File is closed, after opened for writing, check if it can be uploaded."""
        self.notify(Path(event.src_path), event.event_type)

    def notify_all_initially_present_files(self):
        """Check all files in the specified directory if they need to be uploaded."""
        for file_path in self.directory_to_handle.glob("*"):
            self.notify(file_path, "watcher startup")

    @logger.catch(
        Exception,
        message="An error occurred while sending a file event.",
    )
    def notify(self, file_path: Path, source: str):
        """Notify listeners about a modified file."""
        with contextlib.suppress(IsADirectoryError):
            if file_path.name not in self.included_filenames:
                return
            self._wait_for_file_to_be_stable(file_path)
            logger.info(f"File event: {source} → {file_path}")
            for listener in self.listener:
                listener.notify(DataPayload.from_file(file_path))

    def _wait_for_file_to_be_stable(self, file_path: Path) -> bool:
        prev_size = file_path.stat().st_size

        for _ in range(self.max_sec_wait_for_stable_file):
            time.sleep(1)
            curr_size = file_path.stat().st_size
            if prev_size == curr_size:
                return True
            prev_size = curr_size

        msg = (
            f"File {file_path} size not stable after"
            f" {self.max_sec_wait_for_stable_file} seconds."
        )
        raise FileUnstableError(msg)


def get_event_handler(
    settings: FileSyncConfig, listener: list | None
) -> FileSystemEventHandler:
    return ModifiedFileHandler(
        directory_to_handle=settings.sync_path,
        included_filenames=settings.filenames,
        listener=listener,
    )
