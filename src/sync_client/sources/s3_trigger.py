# Sync-Client: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023 PAILOT GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""A Source for the SyncClient that triggers a poll based on a changed S3 file."""
import time

from loguru import logger

from sync_client.data_models import DatabaseOperation, DataLocation, DataPayload, S3File
from sync_client.io.io_s3 import S3Bucket
from sync_client.sources.source_mixin import SourceMixIn


class S3TriggerSource(SourceMixIn):
    """
    A trigger polling for the existence of files.

    Once a change is triggered, a list of files is fetched and passed to a polling obj.
    The downloaded files are deleted from the bucket.
    """

    polling_timeout_seconds = 3

    def __init__(
        self,
        s3: S3Bucket,
        table_names: list[str],
    ) -> None:
        self.s3 = s3
        self.download_files = [
            S3File(table_name=table_name) for table_name in table_names
        ]
        file_names = ", ".join(file.file_name for file in self.download_files)
        logger.success(f"Started S3 source. bucket: {s3.bucket}; files: [{file_names}]")

    def poll(self) -> list[DataPayload]:
        # check for availability of files
        available_files = [
            file
            for file in self.download_files
            if self.s3.is_file_available(remote_path=file.file_name)
        ]
        if not available_files:
            return []

        logger.info("S3 Trigger detected")
        # wait to ensure upload of all files is finished
        time.sleep(self.polling_timeout_seconds)

        # download all files
        to_download: list[DataPayload] = []
        for file in available_files:
            data = self.s3.download_from_s3(remote_path=file.file_name)
            if data:
                self.s3.delete_file_from_s3(remote_path=file.file_name)
            # different behaviour for file and subdirectory
            if file.is_directory:
                payload = DataPayload(
                    data=data,
                    name=file.table_name,
                    source=DataLocation.S3,
                    operation=DatabaseOperation.Update,
                    filter_column="planning_unit",
                    filter_value=file.planning_unit,
                )
            else:
                payload = DataPayload(
                    data=data,
                    name=file.table_name,
                    source=DataLocation.S3,
                    operation=DatabaseOperation.Replace,
                )
            to_download.append(payload)
        return to_download
