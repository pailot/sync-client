Sync-Client: transfer data to and from local files and DB to AWS S3.
Copyright (C) 2023 PAILOT GmbH

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

```mermaid
classDiagram
    class SyncClient {
        + notify(payload: DataPayload)
        + poll_sources()
        + upload_to_s3(payload: DataPayload)
        + write_to_db(payload: DataPayload)
        + run()
    }
    namespace sources {
    class ModifiedFileWatcher {
        + Check for modified files
    }

    class DBTriggerSource {
        + poll()
    }

    class S3TriggerSource {
        + poll()
    }
    }
    class RetryTaskScheduler {
        + add_tasks(tasks: list)
        + append_to_queue(task: tuple)
        + run()
    }

    namespace io {
    class S3Bucket {
        + upload_to_s3(data, name)
    }

    class Engine {
    SQLAlchemy.Engine
    }}

    class Watchdog {
    watchdog.observers.Observer
    }

    SyncClient *-- S3Bucket
    SyncClient *-- Engine

    SyncClient o-- S3TriggerSource: +poll()
    SyncClient o-- ModifiedFileWatcher : +notify()
    SyncClient o-- DBTriggerSource:  +poll()
    SyncClient *-- RetryTaskScheduler: +append_to_queue()
    ModifiedFileWatcher ..> Watchdog

    DBTriggerSource *-- Engine
    S3TriggerSource *-- S3Bucket
```